# 这个新的配置文件继承自一个原始配置文件，只需要突出必要的修改部分即可
# base 是这个py文件的相对路径, 而非项目的绝对路径.
_base_ = '../faster_rcnn/faster_rcnn_r50_fpn_1x_coco.py'

# 我们需要对头中的类别数量进行修改来匹配数据集的标注
model = dict(
    roi_head=dict(
        bbox_head=dict(num_classes=13)))

# 修改数据集相关设置
dataset_type = 'COCODataset'
classes = ('cigarette', 'paper_scraps', 'bucket', 'cleaning_tool', 'fire_fighting', 'no_uniform', 'no_hardhat', 'head_hat', 'hard_hat', 'no_glove', 'emergency_shelter', 'exit', 'noise_harmful')
data = dict(
    train=dict(
        img_prefix='/home/musk/yolo_ly/datasets/trim500_train_test/coco_dataset/images/train2017/',
        classes=classes,
        ann_file='/home/musk/yolo_ly/datasets/trim500_train_test/coco_dataset/annotations/instances_train2017.json'),
    val=dict(
        img_prefix='/home/musk/yolo_ly/datasets/trim500_train_test/coco_dataset/images/val2017/',
        classes=classes,
        ann_file='/home/musk/yolo_ly/datasets/trim500_train_test/coco_dataset/annotations/instances_val2017.json'),
    test=dict(
        img_prefix='/home/musk/yolo_ly/datasets/trim500_train_test/coco_dataset/images/val2017/',
        classes=classes,
        ann_file='/home/musk/yolo_ly/datasets/trim500_train_test/coco_dataset/annotations/instances_val2017.json'))

# 我们可以使用预训练的 Mask R-CNN 来获取更好的性能
load_from = 'ZL/checkpoints/yolov3_mobilenetv2_320_300e_coco_20210719_215349-d18dff72.pth'

