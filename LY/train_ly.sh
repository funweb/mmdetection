# train
python tools/train.py configs/workshop/faster_rcnn_r50_fpn_1x_coco_20200130.py

# test
# python tools/test.py configs/workshop/faster_rcnn_r50_fpn_1x_coco_20200130.py work_dirs/faster_rcnn_r50_fpn_1x_coco_20200130/latest.pth --eval bbox
