#!/bin/bash


# 等待时间
# input:
#	param: seconds
# retur:
#	string: 倒计时
function wait_s {
   for i in $(seq $1 -1 1)
    do
        echo -ne "wait: \e[31m $i \e[0m s\r"
        sleep 1
    done
}



# 遍历文件夹下的文件
# input
#	@param: 待遍历的文件夹
# return: 数组
function dir_array {
    #1.待遍历的文件夹
    directory="$1"

    suffix=".py"

    #2.遍历子目录
    count=-1  # 文件计数
    for file in `ls $directory`
    do
            if [ -f $directory/$file ];
            then
                    count=$((${count} + 1));
                    A[$count]=${file%${suffix}}  # 去除后缀.py  # attention: prefix: #, suffix: %
            fi
    done

    echo ${A[*]}
}


# 数组转化为字符串输出
# input:
# 	param: 数组
# 	高亮的索引
# return:
# 	字符串
function concat_array2string {
    index=0
    string="[ "
    for i in $1; do
        if [ $index -eq $2 ]; then
            string="   ${string}\e[31m ${index}:${i} \e[0m | "
        else
            string="   ${string}${index}:${i} | "
        fi
        ((index=index+1))
    done
    string="${string:0:0-2} ]"
    echo $string
}


files=$(dir_array configs/workshop)  # string

files_array=(${files//,/})  # array

default_mode_index=0  # default param
default_config_index=0  # default param

mode_array=("train" "test")

echo -en "Please select: $(concat_array2string "${mode_array[*]}" ${default_mode_index}) :\t"; read -t 15 mode_index
echo -en "Please select: $(concat_array2string "${files_array[*]}" ${default_config_index}) :\t"; read -t 15 config_index

default_mode_index=${mode_index:-${default_mode_index}}  # default
default_config_index=${config_index:-${default_config_index}}  # default

# echo -e "${mode_array[${default_mode_index}]}"
# echo -e "${files_array[${default_config_index}]}"

if test ${mode_array[${default_mode_index}]} = "train" ; then
    runs_string="python tools/train.py configs/workshop/${files_array[${default_config_index}]}.py"
elif test ${mode_array[${default_mode_index}]} = "test" ; then
    runs_string="python tools/test.py configs/workshop/${files_array[${default_config_index}]}.py work_dirs/${files_array[${default_config_index}]}/latest.pth --eval bbox"
else
    exit 1
fi

echo ${runs_string}

wait_s 5
${runs_string}

