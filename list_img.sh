#!/bin/bash


# 等待时间
# input:
#	param: seconds
# retur:
#	string: 倒计时
function wait_s {
   for i in $(seq $1 -1 1)
    do
        echo -ne "wait: \e[31m $i \e[0m s\r"
        sleep 1
    done
}


# 遍历文件夹下的文件
# input
#	@param: 待遍历的文件夹
# return: 数组
function dir_array {
    #1.待遍历的文件夹
    directory="$1"

    #2.遍历子目录
    count=-1  # 文件计数
    for file in `ls $directory`
    do
            if [ -f $directory/$file ];
            then
                    count=$((${count} + 1));
                    A[$count]=${file}  # 去除后缀.py  # attention: prefix: #, suffix: %

            fi
    done

    echo ${A[*]}
}

index_dir="/home/musk/video_bak/view/frames/plane_frames_list"
file_string=$(dir_array ${index_dir})

for file_name in ${file_string};
do
    echo ${file_name};
    wait_s 3;
    python demo/image_demo_list.py ${index_dir}/${file_name} configs/faster_rcnn/faster_rcnn_r101_fpn_mstrain_3x_coco.py checkpoints/faster_rcnn_r101_fpn_mstrain_3x_coco_20210524_110822-4d4d2ca8.pth /home/musk/video_bak/view/frames/plane_out/faster/
done





